package main

import (
    "fmt"
    "time"
    "sync"
)

func main() {
	// sync()
	// async()

	// チャネル
	var waitGroup sync.WaitGroup

	go Myasync()
	waitGroup.Wait()
	Mysync()

}

func Mysync(){
    fmt.Println("started.")

    // 1秒かかるコマンド
    fmt.Println("sleep1 started.")
    time.Sleep(1 * time.Second)
    fmt.Println("sleep1 finished.")

    // 2秒かかるコマンド
    fmt.Println("sleep2 started.")
    time.Sleep(2 * time.Second)
    fmt.Println("sleep2 finished.")

    // 3秒かかるコマンド
    fmt.Println("sleep3 started.")
    time.Sleep(3 * time.Second)
    fmt.Println("sleep3 finished.")

    fmt.Println("Mysync all finished.")
}

func Myasync() {
    fmt.Println("started.")

    // チャネル
    sleep1_finished := make(chan bool)
    sleep2_finished := make(chan bool)
    // sleep3_finished := make(chan bool)

    go func() {
        // 1秒かかるコマンド
        fmt.Println("sleep1 started.")
        time.Sleep(1 * time.Second)
        fmt.Println("sleep1 finished.")
        sleep1_finished <- true
    }()

    go func() {
        // 2秒かかるコマンド
        fmt.Println("sleep2 started.")
        time.Sleep(2 * time.Second)
        fmt.Println("sleep2 finished.")
        sleep2_finished <- true
    }()

    // go func() {
    //     // 3秒かかるコマンド
    //     fmt.Println("sleep3 started.")
    //     time.Sleep(3 * time.Second)
    //     fmt.Println("sleep3 finished.")
    //     sleep3_finished <- true
    // }()

    // 終わるまで待つ
    <- sleep1_finished
    <- sleep2_finished
    // <- sleep3_finished

    fmt.Println("Myasync all finished.")
}
