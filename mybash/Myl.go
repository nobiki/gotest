package mybash

import (
	"fmt"
	"log"
	"os/exec"
)

func Myls() {
	log.Print("コマンドを実行するプログラム")
	out,error := exec.Command("ls", "-la").Output()

	if error != nil {
		log.Printf("エラー：%s", error)
	}else{
		fmt.Printf("%s", out)
		// fmt.Println("現在時刻：%s", out)
	}
}
